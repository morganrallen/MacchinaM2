#ifndef CAN_H_
#define CAN_H_

#include <Arduino.h>
#include <due_can.h>

#define CAN0_SPEED  (500000)
#define CAN0_ENABLE (255)
#define CAN1_ENABLE (48)

extern bool can0_running;
void can_stop();
void can_start();
void can_handle_data();

extern bool can1_running;
void can1_stop();
void can1_start();
void can1_handle_data();

#endif
