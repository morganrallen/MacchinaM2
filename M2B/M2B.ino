#include "M2B.h"
#include "SamNonDuePin.h"
#include "can.h"

#define RESTART_ESP32 (false)

bool led = 0;
bool toggleBootMode = false;
bool passThrough = false;
bool pipe = false;
bool passThroughStarted = false;

int doDelay = 0;

uint32_t passThroughLastEvent = 0;

const int XB_nRST = X4;
const int XB_GPIO0 = 56;
const int LED_YELLOW = X0;

void setup()
{
  SerialUSB.begin(234000);
	Serial.begin(115200);

  SerialUSB.println(printf("System Reset"));
  SerialUSB.println("> ");

  pinModeNonDue(XB_nRST, OUTPUT);
  pinModeNonDue(XB_GPIO0, OUTPUT);

  digitalWriteNonDue(XB_nRST, HIGH);
  digitalWriteNonDue(XB_GPIO0, HIGH);

  pinModeNonDue(LED_YELLOW, OUTPUT);
  digitalWriteNonDue(LED_YELLOW, LOW);

  if(RESTART_ESP32) {
    // reboot ESP32 on startup
    digitalWriteNonDue(XB_nRST, LOW);

    toggleBootMode = true;
    doDelay = 500;

  }

  delay(1000);

  Serial.write("p");
}

void printCanFrame(CAN_FRAME frame) {
  uint8_t buf[22];

  if (frame.extended)
  {
    SerialUSB.print("T");
    sprintf((char *)buf, "%08x", frame.id);
    SerialUSB.print((char *)buf);
  }
  else
  {
    SerialUSB.print("t");
    sprintf((char *)buf, "%03x", frame.id);
    SerialUSB.print((char *)buf);
  }
  SerialUSB.print(frame.length);
  for (int i = 0; i < frame.length; i++)
  {
    sprintf((char *)buf, "%02x", frame.data.byte[i]);
    SerialUSB.print((char *)buf);
  }

  uint16_t timestamp = (uint16_t)millis();
  sprintf((char *)buf, "%04x", timestamp);
  SerialUSB.print((char *)buf);

  SerialUSB.print('\n');
}

uint32_t lastMilli = 0;

void setReset() {
  SerialUSB.println("\n\nReseting ESP32");

  digitalWriteNonDue(XB_nRST, LOW);

  toggleBootMode = true;
  doDelay = 500;
}

void loop()
{
  if(doDelay > 0) {
    SerialUSB.print("Delay: ");
    SerialUSB.print(doDelay);

    delay(doDelay);

    SerialUSB.print(".\n> ");

    doDelay = 0;
  }

  if(passThroughLastEvent > 0 && millis() - passThroughLastEvent > 3000) {
    SerialUSB.print(millis());
    SerialUSB.print(" - ");
    SerialUSB.print(passThroughLastEvent);
    SerialUSB.print(" = ");
    SerialUSB.println(millis()-passThroughLastEvent);
    passThroughLastEvent = 0;
    passThrough = false;
    digitalWriteNonDue(XB_GPIO0, HIGH);

    setReset();
  }

  if(toggleBootMode) {
    digitalWriteNonDue(XB_nRST, HIGH);
    toggleBootMode = false;
    Serial.begin(115200);
  }

  CAN_FRAME incomingFrame;
  bool doTransmit = false;

  if(can0_running) {
    Can0.read(incomingFrame);
    doTransmit = true;
  }

  if(can1_running) {
    Can1.read(incomingFrame);
    doTransmit = true;
  }

  if(doTransmit) {
    //printCanFrame(incomingFrame);

    if(millis() - lastMilli > 1000) {
      SerialUSB.println("\nTransmitting\n");

      printCanFrame(incomingFrame);

      int len = sizeof(CAN_FRAME);
      unsigned char * buf = (unsigned char *)malloc(len);
      memcpy(buf, &incomingFrame, len);

      Serial.write(buf, len);
      Serial.write('\n');

      lastMilli = millis();
    }
  }

  // this is needed if serialEventUSB doesn't exist in the Due variant
  if(SerialUSB.available()) serialEventUSB();
}

void serialEvent() {
  // 'activity' light
  digitalWriteNonDue(LED_YELLOW, led ? HIGH : LOW);
  led = led ? 0 : 1;

  // Serial data from XB_ can just be passed straight through
  while(Serial.available()) {
    SerialUSB.write((uint8_t)Serial.read());
  }
}

void serialEventUSB() {
  // 'activity' light
  digitalWriteNonDue(LED_YELLOW, led ? HIGH : LOW);
  led = led ? 0 : 1;

  while(SerialUSB.available()) {
    uint8_t c = SerialUSB.read();

    // this needs to be rethought, knock sequence? dunno but
    // breaks on real data
    if(pipe && c == 0x03) {
      pipe = false;
      SerialUSB.println("Ending pipe");

      return;
    }

    // if already in flash mode just write and stop
    if(pipe || passThrough) {
      Serial.write(c);

      if(passThrough) {
        passThroughLastEvent = millis();
      }

      return;
    }

    // proper data coming back from ESP32 can be passed back
    SerialUSB.write(c);
    SerialUSB.write("\n");

    // simple command set
    // b: enter boot mode
    // r: reset

    // both b and r lower nRST
    // and toggle bootmode (back up in 500ms)
    if(c == 'b' || c == 'r') {
      setReset();
    }

    // b also lowers GPIO0 and enables pass-through
    if(c == 'b') {
      Serial.begin(234000);

      digitalWriteNonDue(XB_GPIO0, LOW);

      passThrough = true;
    } else if(c == 'p') {
      SerialUSB.println("\nPiping data to ESP32");
      pipe = true;
    } else if(c == 's') {
      // start can bus
      if(can0_running) {
        SerialUSB.println("Stopping CAN0");
        can_stop();
      } else {
        SerialUSB.println("Starting CAN0");
        can_start();
      }
    } else if(c == 'S') {
      // start can bus
      if(can1_running) {
        SerialUSB.println("Stopping CAN1");
        can1_stop();
      } else {
        SerialUSB.println("Starting CAN1");
        can1_start();
      }
    } else if(c == '\n') {
      SerialUSB.print("> ");
    }
  }
}
