#include "can.h"

bool can0_running = false;
bool can1_running = false;

void can1_stop() {
  Can0.disable();
  can1_running = false;
}

void can1_handle_data() {
  CAN_FRAME frame;
  uint8_t buf[22];

  if(Can1.available()) Can0.read(frame);

  if (frame.extended)
  {
    SerialUSB.print("T");
    sprintf((char *)buf, "%08x", frame.id);
    SerialUSB.print((char *)buf);
  }
  else
  {
    SerialUSB.print("t");
    sprintf((char *)buf, "%03x", frame.id);
    SerialUSB.print((char *)buf);
  }
  SerialUSB.print(frame.length);
  for (int i = 0; i < frame.length; i++)
  {
    sprintf((char *)buf, "%02x", frame.data.byte[i]);
    SerialUSB.print((char *)buf);
  }

  uint16_t timestamp = (uint16_t)millis();
  sprintf((char *)buf, "%04x", timestamp);
  SerialUSB.print((char *)buf);

  SerialUSB.println("");
}

void can1_start() {
  Can1.enable();
  Can1.begin(125000, CAN1_ENABLE);

  can1_running = true;
}

void can_stop() {
  Can0.disable();
  can0_running = false;
}

void can_start() {
  Can0.enable();
  Can0.begin(CAN0_SPEED, CAN0_ENABLE);

  can0_running = true;
}

void can_handle_data() {
  CAN_FRAME frame;
  uint8_t buf[22];

  if(Can0.available()) Can0.read(frame);

  if(frame.id != 0x0514) {
    return;
  }
  if (frame.extended)
  {
    SerialUSB.print("T");
    sprintf((char *)buf, "%08x", frame.id);
    SerialUSB.print((char *)buf);
  }
  else
  {
    SerialUSB.print("t");
    sprintf((char *)buf, "%03x", frame.id);
    SerialUSB.print((char *)buf);
  }
  SerialUSB.print(frame.length);
  for (int i = 0; i < frame.length; i++)
  {
    sprintf((char *)buf, "%02x", frame.data.byte[i]);
    SerialUSB.print((char *)buf);
  }

  uint16_t timestamp = (uint32_t)millis();
  sprintf((char *)buf, "%08x", timestamp);
  SerialUSB.print((char *)buf);

  SerialUSB.println("");
}
