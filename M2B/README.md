Macchina M2-B
=============

Features
--------
* Bootloader control for ESP32
* Pass-through flash for ESP32
* Control CAN Busses
* Send CAN Frames to UART or BLE

Usage
-----
Connecting to the serial port will provide a very basic console.

Commands
--------

### `r`
Reboot ESP32

### `b`
Reboot ESP32 into flash mode

### `s`
Toggle CAN0

### `S`
Toggle CAN1

### `p`
Enable pass-though mode. All data will be piped to the ESP32. `^C` to stop.
