var fs = require("fs");
var data = "";

console.log("Time Stamp,ID,Extended,Dir,Bus,LEN,D1,D2,D3,D4,D5,D6,D7,D8");

var rs = fs.createReadStream(process.argv[2]).on("data", function(c) {
  rs.pause();

  data += c.toString();
  var parts = data.split("\n");
  data = parts.pop();

  var lastBytes = "";

  parts.forEach(function(data) {
    if(data[0] === "t") return;

    var id = parseInt(data.slice(1,9));
    var len = parseInt(data.slice(9, 10));
    var pad = (new Array(9 - ("" + id).length)).join(" ");

    var bytes = [];

    for(var i = 10; i < 26; i += 2) {
      bytes.push(data.slice(i, i + 2));
    }

    var ts = data.slice(26, 30);

    // Time Stamp,ID,Extended,Dir,Bus,LEN,D1,D2,D3,D4,D5,D6,D7,D8,
    // 535618888,00000514,false,Rx,0,8,00,00,FE,06,08,80,01,80,
    //
    //console.log(" %s%s%s%s%s", pad, id, len, bytes.join(""), ts);

    if(lastBytes.length > 0) {
      if(bytes.toString() !== lastBytes)
        console.log("00000%s,%s,false,Rx,0,%s,%s", (ts), pad.replace(/ /g, "0") + id, len, bytes.join(",").toUpperCase());
    }

    lastBytes = bytes.toString();
  });

  rs.resume();
});
