var async = require("async");
var fs = require("fs");
var noble = require("noble");
var udevSerial = require("udev-serial");
var test = require("tape");

var desc = {
  ID_MODEL: "Arduino_Due",
  SUBSYSTEM: "tty"
}

var serviceUUIDs = [ "cdab" ];
var charUUIDs = [ "ff01" ];
var dev;
var fifo;
var sp;

var serial = udevSerial(desc, {
  baudRate: 115200
});

test(function(t) {
  t.plan(2);

  serial.on("open", function(serialport) {
    sp = serialport;

    fifo = fs.createWriteStream("/tmp/debug");

    sp.on("data", function(c) {
      fifo.write(c);
    });

    async.series([
      function(next) {
        sp.once("data", function(c) {
          next();
        });

        setTimeout(function() {
          sp.write(Buffer.from([ 0x03 ]));
        }, 300);
      }, function(next) {
        sp.once("data", function(c) {
          t.equal(c.toString()[0], "p", "p echoed");
          t.ok(c.toString().indexOf("Piping") > -1, "Entered piping");

          next();
        });

        sp.write("p");
      }
    ], function() {
      //sp.write(Buffer.from([0x03]));
      //sp.write("r");

      t.end();
    });
  });
}, "serial connection");

test(function(t) {
  t.plan(1);

  noble.on("discover", function(device) {
    dev = device;

    console.log("uuid: %s", device.uuid);

    device.on("servicesDiscover", function(char) {
      //console.log(char);
    });

    async.series([function(next) {
      device.once("connect", function() { setTimeout(next, 300); });
      device.connect();

      device.on("disconnect", function(err) {
        console.trace("disconnect", err);
      });
    }, function(next) {
      console.log("connected");

      device.discoverSomeServicesAndCharacteristics([ "4d32" ], [ "ff01" ], function(err, services, chars) {
        var char = chars[0];
        var msg = (new Date()).valueOf().toString();

        char.on("data", function(data) {
          t.equal(msg, data.toString(), "got data");
          t.end();
        });

        char.subscribe(function() {
          console.log("subscribed");

          setTimeout(function() {
            sp.write(msg);
          }, 300);
          setTimeout(function() {
            sp.write("\n");
          }, 600);
        });
      });

      next();
    }], function(err) {
      if(err)
        throw err;
    });
  });

  async.series([ function(next) {
    console.log("state: %s", noble.state);
    if(noble.state !== "poweredOn")
      noble.once("stateChange", function() {
        next();
      });
    else next();
  }, function(next) {
    noble.startScanning(serviceUUIDs, false, next);
  }], function() {
    console.log("scanning...");
  });
}, "ble");

test.onFinish(function() {
  console.log("bye!");
  sp.close();
  noble.stopScanning();
  dev.removeAllListeners();
  dev.disconnect();
  fifo.end();

  process.exit();
});
