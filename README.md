Macchina M2-B
=============

Repo contains the work I've managed so far the Macchina M2 and ESP32.

Contents
--------

### `/M2B/`

This is the Arduino firmware running on the M2-B. It's main entry is
`/M2B/M2B.ino`. See README in directory for more info.

### `/mac32/`

`esp-idf` based firmware to handle Bluetooth Low Energy communication
between the M2 and another BLE device (phone, laptop). See README in
directory for more info.

### Utilities

If you have `node` and `npm` available there are some basic testing
tools and unit tests. Install dependencies and run tests.

```
$ npm install
$ sudo npm test # sudo probably needed

TAP version 13
# serial connection
ok 1 p echoed
ok 2 Entered piping
# ble
state: poweredOn
scanning...
uuid: 240ac4021ade
connected
subscribed
ok 3 got data
bye!

1..3
# tests 3
# pass  3

# ok
```
