M2-B ESP32 Firmware
===================

Gateway for M2-B BLE communications.

Update serial port using `make menuconfig` under `Serial flasher config`.
Then use `make flash`
